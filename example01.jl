using MixedModels
using DataFrames

d = readtable("aldayetal2014.tab",separator='\t',makefactors=true);

d[:subj] = UTF8String[string(x) for x in d[:subj]]; 
d[:item] = UTF8String[string(x) for x in d[:item]];
pool!(d,[:subj,:item])

n400 = d[d[:win] .== "N400",:];

@time m = fit(lmm(mean ~ 1 + wordOrder*ambiguity*np1type*np2type + (1|subj) + (1|item), n400));
@time m = fit(lmm(mean ~ 1 + wordOrder*ambiguity*np1type*np2type + (1|subj) + (1|item), n400));
@time m2 = fit(lmm(mean ~ 1 + wordOrder*ambiguity*np1type*np2type +
                             (1+wordOrder*ambiguity+np1type+np2type|subj) +
                             (1+wordOrder*ambiguity+np1type+np2type|item)
                           ,data=n400));
